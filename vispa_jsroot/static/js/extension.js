require.config({
  paths: {
    JSRootCore     : vispa.dynamicURL("extensions/jsroot/static/vendor/jsroot/scripts/JSRootCore"),
    JSRootPainter  : vispa.dynamicURL("extensions/jsroot/static/vendor/jsroot/scripts/JSRootPainter"),
    d3             : vispa.dynamicURL("extensions/jsroot/static/vendor/jsroot/scripts/d3.v3.min"),
    MathJax        : vispa.dynamicURL("extensions/jsroot/static/vendor/mathjax/MathJax.js?config=TeX-AMS-MML_SVG&amp;delayStartupUntil=configured")
  },
  map: {
    '*' : {
      "jquery-ui" : "vendor/jquery/plugins/ui/jquery.ui",
      touchpunch  : "vendor/jquery/plugins/touchpunch/jquery.touchpunch"
    }
  }
});
define([
  "vispa/extension",
  "vispa/views/main",
  "vispa/utils",
  "text!../html/main.html",
  "css!../css/styles",
  "css!../vendor/jsroot/style/JSRootPainter",
  "css!../vendor/jsroot/style/JSRootGeoPainter",
], function(Extension, MainView, Utils, template) {

  var JsROOTExtension = Extension._extend({
    init: function init() {
      init._super.call(this, "jsroot", "ROOT file viewer");
      this.mainMenuAdd([
        this.addView(JsROOTView)
      ]);
    },
  });

  var JsROOTView = MainView._extend({
    init: function init(args) {
      init._super.apply(this, arguments);

      this.state.setup({
        path: undefined
      }, args);

      require([
        "JSRootCore",
        "JSRootPainter",
        "MathJax",

      ]);

      this.painter = null;
      this.nodes   = {};

      this.prefs.watch("sidebarWidth", function(sidebarWidth){
        if (!this.nodes.$main) return;
        if (this.nodes.$sidebar.width() === sidebarWidth) return;

        this.nodes.$sidebar.css({
          left : 0,
          width: sidebarWidth
        });
        this.nodes.$content.css({
          left : sidebarWidth,
          width: ""
        });
      });

      // look if file has been deleted or modified
      this.socket.on("watch", function(data) {
        if (data.watch_id != "jsroot")
          return;
        // in case file deleting or renamings
        var filename = ((data.path).split('/')).pop();
        if (data.event == "vanish") {
          this.alert("The file '" + filename + "' has been deleted or renamed." +
            "\nThis ROOT viewer will be frozen.");
        }
        if (data.event == "modify") {
          this.confirm("The file '" + filename + "' has been modified." +
            "\n Would you like to reload it?", function(res) {
            if (!res)
              this.close();
            else
              this.spawnInstance("jsroot", "JsROOTView", {
                path: this.state.get("path"),
              },{
                replace: this,
              });
          }.bind(this));
        }
      }.bind(this));
    },

    getFragment: function() {
      return this.state.get("path") || "";
    },

    applyFragment: function(fragment) {
      this.state.set("path", fragment);
    },

    render: function($node) {
      // set icon for tab
      this.icon = "jsroot-icon-tab";

      // append template to node
      var $main    = $(template).appendTo($node);
      var $sidebar = $main.find(".sidebar-resize-wrapper");
      var $content = $main.find(".content-wrapper");

      // make divs resizable
      $sidebar.resizable({
        start: function() {
          var mainWidth  = $main.width();
          $sidebar.resizable("option", "grid", [mainWidth * 0.01, 1]);
          $sidebar.resizable("option", "minWidth", 10);
          $sidebar.resizable("option", "maxWidth", mainWidth-10);
        },
        resize: function() {
          var mainWidth    = $main.width();
          var sidebarWidth = $sidebar.width();
          var contentWidth = mainWidth - sidebarWidth;
          $sidebar.css({
            left : 0,
            width: sidebarWidth
          });
          $content.css({
            left : sidebarWidth,
            width: contentWidth
          });
        },
        stop: function() {
          this.prefs.set("sidebarWidth", parseInt(Math.floor($sidebar.width())));
        }.bind(this),
      });

      // store nodes
      this.nodes.$main        = $main;
      this.nodes.$sidebar     = $sidebar;
      this.nodes.$content     = $content;

      this.openFile(this.getState("path"));
    },

    openFile: function(path) {
      if (!path || !this.nodes.$main) return;

      this.loading = true;
      this.state.set("path", path);
      this.setLabel(path, true);

      require(["JSRootPainter"], function(JSROOT) {
        JSROOT.MathJax = 2;

        // set new ids
        var treeId = "tree-" + Utils.uuid();
        var padId  = "pad-"  + Utils.uuid();

        this.nodes.$main.find(".tree").attr("id", treeId);
        this.nodes.$main.find(".pad").attr("id", padId);

        // build the file path
        var ext = path.split(".").pop().toLowerCase();
        path = "fs/getfile?_workspaceId=" + this.workspaceId + "&path=" + path;
        path = vispa.dynamicURL(path);

        // open root files
        if (ext == "root") {
          // tree
          this.painter = new JSROOT.HierarchyPainter("painter-" + Utils.uuid(), treeId);
          JSROOT.RegisterForResize(this.painter);
          // main div
          this.painter.SetDisplay("flex", padId);
          this.painter.OpenRootFile(path, function() {
            this.loading = false;
          }.bind(this));
        }
        // open json files
        else {
          JSROOT.NewHttpRequest(path, 'object', function(obj) {
            JSROOT.draw(padId, obj, "hist");
            this.loading = false;
          }.bind(this)).send();
        }

      }.bind(this));

      // watch
      this.POST("/ajax/fs/watch", {
        path    : this.getState("path"),
        watch_id: "jsroot"
      });

    },

    openHelpDialog: function() {
      var supportedClasses = ["TH1, TH2, TH3","TProfile","TGraph","TMultiGraph","TF1, TF2, TF3","TList","TAxis","TPave","TObject","TCanvas","TGaxis","TFrame","TGeo","TFile","TFolder"];
      this.alert(
        "VISPA adopts the <a target='_blank' href='https://github.com/linev/jsroot'>jsroot </a>" +
        " library to draw <a target='_blank' href='https://root.cern.ch'>ROOT</a> objects. Many" +
        " classes like <ul><li>" +
        supportedClasses.join("</li><li>") +
        "</li></ul> are supported, as well as LaTeX strings.<br>For more information, visit the " +
        "jsroot <a target='_blank' " +
        "href='https://github.com/linev/jsroot/blob/master/docs/JSROOT.md'> documentation site</a>.",
      {
        title   : "Why are my root files not shown correctely?",
      });
    },

    onClose: function() {
      this.POST("/ajax/fs/unwatch");
    }

  },{
    iconClass: "fa-area-chart",
    label: "ROOT file viewer",
    menuPosition: 200,
    menuFFcallback: function(path) {
      if (!path) return;
      if (!~["root","json"].indexOf(path.split(".").pop().toLowerCase())) {
        vispa.messenger.alert("The selected file is not a valid root or json file!");
      } else {
        return {path: path};
      }
    },
    name: "JsROOTView",
    preferences: {
      items: {
        sidebarWidth: {
          level: 3,
          label: "Sidebar width",
          type : "integer",
          value: 200,
          range: [50, 1500, 25],
        },
      },
    },
    menu: {
      help: {
        label      : "Help",
        iconClass  : "fa-question",
        callback: function() {
          this.$root.instance.openHelpDialog();
        }
      }
    },
    fileHandlers: [
      {
        label: "ROOT file viewer",
        iconClass: "fa-area-chart",
        position: function () {
          return {
            root: 1,
            json: 1010,
          }[this.value.split(".").pop().toLowerCase()];
        },
        hidden: function () {
          return this.position===undefined;
        },
        callback: function () {
          (this.$root && this.$root.view || vispa).spawnInstance("jsroot", "JsROOTView",
            {path: this.value});
        }
      }
    ],
  });

  return JsROOTExtension;
});
